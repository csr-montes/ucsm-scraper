<?php

class University{

	/**
     * CURL structure for POST request.
     *
	 * @param  string  $data
     * @return json
     */
	public function curlTemplatePost($url, $data)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_TIMEOUT,10);

		$headers = array();
		$headers[] = "Content-Type: application/json; charset=utf-8";
		$headers[] = "dataType: text";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		try {
			$result = curl_exec($ch);
		} catch (Exception $ch) {
			echo 'Error:' . curl_error($ch);
			$result = "PASARON 30SEG";
		}

		curl_close ($ch);

		return json_decode($result);
	}

	public function getCareer()
	{
		$url = "https://academico.ucsm.edu.pe:444/MatriculaService/MatriculaService.svc/DevolverTodasProyectosLectivo";
		return $this->curlTemplatePost($url ,"{\"documento\":\"70336257\", \"seguridad\":\"ABCDE\"}");
	}
	public function getSemester($unidadAcademica, $tipoProyectoLectivo)
	{
		$url = "https://academico.ucsm.edu.pe:444/MatriculaService/MatriculaService.svc/DevolverListaSemetres";
		return $this->curlTemplatePost($url ,"{\"unidadAcademica\":\"$unidadAcademica\", \"iTipPro\":\"$tipoProyectoLectivo\", \"seguridad\":\"ABCDE\"}");
	}
	public function getCourseData($unidadAcademica, $semestre)
	{
		$url = "https://academico.ucsm.edu.pe:444/MatriculaService/MatriculaService.svc/ObtenerCursosSemestrePrograma";
		return $this->curlTemplatePost($url ,"{\"unidadAcademica\":\"$unidadAcademica\", \"semestre\":\"$semestre\", \"cad\":\"ABCDE\"}");
	}

	public function getSchedule($unidadAcademica, $semestre, $codigoCurso)
	{
		$url = "https://academico.ucsm.edu.pe:444/MatriculaService/MatriculaService.svc/ObtenerHorariosSemestreCurso";
		return $this->curlTemplatePost($url ,"{\"unidadAcademica\":\"$unidadAcademica\", \"codigoCurso\":\"$codigoCurso\", \"semestre\":\"$semestre\", \"cad\":\"ABCDE\"}");
	}

	/**
     * Get photo.
     *
     * @return curlTemplatePost
     */
	public function getPhoto()
	{
		// https://sigeva.ucsm.edu.pe:444/MatriculaService/MatriculaService.svc/ObtenerUsuarioSancionado
		$url = "https://academico.ucsm.edu.pe:444/ReportesService/ReporteConsultaHorarios.svc/ObtenerFotoAlumno";
		$data = $this->curlTemplatePost($url ,"{\"codigoPersona\":85133, \"seguridad\":\"ABCDE\"}");
		$strDecode = json_decode($data, true);
		echo '<img src="data:image/jpg;base64,' . $strDecode["ObtenerFotoAlumnoResult"][0]["fotoPersona"] . '" />';
		return $data;
	}
}
